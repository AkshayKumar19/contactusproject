<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
    <%
        response.setHeader("cache-control", "no-cache, no-store, must-revalidate");
        response.setHeader("Pregma", "no-cache");
        response.setHeader("Expires", "0");
        if(session.getAttribute("user")==null){
        	response.sendRedirect("login.jsp");
        }
    %>
    Welcome, ${user}<br><br>
    <form action="DashboardServlet" method = "get">
        <input type = "submit" value = "FetchData"><br><br>
    </form> 
    
    <form action="SearchServlet" method = "post">
        isActive: <input type = "text" name = "isActive"><br>
        <input type = "submit" value = "Search"><br><br>
    </form>
    <%
        request.getAttribute("editStatusMessage");
    %>
    ${editStatusMessage}<br>
    <form action="UpdateStatusServlet" method = "post">
        QueryId: <input type = "text" name = "id"><br>
        Set Status: <input type = "text" name = "setStatus"><br>
        <input type = "submit" value = "SetStatus"><br><br>
    </form>
    <form action="LogoutServlet" method ="get">
        <input type = "submit" value = "Logout"><br>
    </form>
</body>
</html>