package com.akshay;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.SQLException;

import com.akshay.dao.RequestDao;

@WebServlet("/UpdateStatusServlet")
public class UpdateStatusServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setHeader("cache-control", "no-cache, no-store, must-revalidate");
		response.setHeader("Pregma", "no-cache");
		response.setHeader("Expires", "0");
		
		HttpSession session = request.getSession();
		if(session.getAttribute("user") == null) {
			response.sendRedirect("login.jsp");
		}
		
		int queryId = Integer.parseInt(request.getParameter("id"));
		Boolean isActive = Boolean.parseBoolean(request.getParameter("setStatus"));
		try {
			editStatus(request, response, isActive, queryId);
		} catch (ClassNotFoundException | SQLException | ServletException | IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void editStatus(HttpServletRequest req, HttpServletResponse res, Boolean isActive, int queryId) throws ClassNotFoundException, SQLException, ServletException, IOException {
		RequestDispatcher requestDispatch = req.getRequestDispatcher("dashboard.jsp");
		String editMessage = "";
		int status = RequestDao.updateIsActiveStatus(isActive, queryId);
		if(status > 0) {
			editMessage = "Successfully updated!";
			
		}
		else {
			editMessage = "Server Error. Please! Try again later";
		}
		req.setAttribute("editStatusMessage", editMessage);
		requestDispatch.forward(req, res);
	}

}
