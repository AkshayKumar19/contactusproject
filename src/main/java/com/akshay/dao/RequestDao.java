package com.akshay.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.directory.SearchResult;

import com.akshay.classFiles.Request;

public class RequestDao {
	private static String URL = "jdbc:postgresql://localhost:5432/guest";
	private static String USER = "guest";
	private static String PASSWORD = "guest123";
	
	public static Connection getConnection() throws ClassNotFoundException, SQLException {

		Class.forName("org.postgresql.Driver");
		Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
		return con;
	}
	
	public static int saveRequest(Request re) throws ClassNotFoundException, SQLException {
		int status = 0;
		Connection con = RequestDao.getConnection();
		String saveRequestQuery = "insert into contactus values (?,?,?,?,?)";
		PreparedStatement statement = con.prepareStatement(saveRequestQuery);
		statement.setString(1, re.getName());
		statement.setString(2, re.getEmail());
		statement.setString(3, re.getPhone());
		statement.setString(4, re.getMessage());
		statement.setBoolean(5, re.getIsActive());
		status = statement.executeUpdate();
		
		return status;
	}
	
	public static List<Request> fetchAllData() throws ClassNotFoundException, SQLException{
		List<Request> queryDetails = new ArrayList<>();
		
		Connection con = RequestDao.getConnection();
		String fetchDataQuery = "select * from contactus";
		PreparedStatement statement = con.prepareStatement(fetchDataQuery);
		ResultSet fetchData = statement.executeQuery();
		while(fetchData.next()) {
			Request reqObject = new Request();
			reqObject.setId(fetchData.getInt(6));
			reqObject.setName(fetchData.getString(1));
			reqObject.setEmail(fetchData.getString(2));
			reqObject.setPhone(fetchData.getString(3));
			reqObject.setMessage(fetchData.getString(4));
			reqObject.setIsActive(fetchData.getBoolean(5));
			queryDetails.add(reqObject);
		}
		return queryDetails;
	}
	
	public static List<Request> searchByStatus(Boolean isActive) throws ClassNotFoundException, SQLException{
		List<Request> searching = new ArrayList<>();
		
		Connection con = RequestDao.getConnection();
		String searchQuery = "select * from contactus where isActive = ?";
		PreparedStatement statement = con.prepareStatement(searchQuery);
		statement.setBoolean(1, isActive);
		ResultSet searchResult = statement.executeQuery();
		while(searchResult.next()) {
			Request reqObject = new Request();
			reqObject.setId(searchResult.getInt(6));
			reqObject.setName(searchResult.getString(1));
			reqObject.setEmail(searchResult.getString(2));
			reqObject.setPhone(searchResult.getString(3));
			reqObject.setMessage(searchResult.getString(4));
			reqObject.setIsActive(searchResult.getBoolean(5));
			searching.add(reqObject);
		}
		return searching;
	}
	
	public static int updateIsActiveStatus(Boolean isActive, int userId) throws ClassNotFoundException, SQLException {
		int status = 0;
		
		String updateStatusQuery = "update contactus set isActive = ? where queryid = ?";
		Connection con = RequestDao.getConnection();
		PreparedStatement statement = con.prepareStatement(updateStatusQuery);
		statement.setBoolean(1, isActive);
		statement.setInt(2, userId);
		status = statement.executeUpdate();
		
		return status;
	}
}