package com.akshay.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao {
	private static String URL = "jdbc:postgresql://localhost:5432/guest";
	private static String USER = "guest";
	private static String PASSWORD = "guest123";
	
	public Boolean check(String user, String pass) throws ClassNotFoundException, SQLException {
		Class.forName("org.postgresql.Driver");
		Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
		String userCheckQuery = "select * from login where uname = ? and pass = ?";
		PreparedStatement statement = con.prepareStatement(userCheckQuery);
		statement.setString(1, user);
		statement.setString(2, pass);
		ResultSet userFetchedData = statement.executeQuery();
		if(userFetchedData.next()) {
			return true;
		}
		return false;
	}
}
