package com.akshay;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import com.akshay.classFiles.Request;
import com.akshay.dao.RequestDao;

@WebServlet("/DashboardServlet")
public class DashboardServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setHeader("cache-control", "no-cache, no-store, must-revalidate");
	    
	    response.setHeader("Pregma", "no-cache");
	   
	    response.setHeader("Expires", "0");
	    
	    HttpSession session = request.getSession();
	    if(session.getAttribute("user") == null){
	    	response.sendRedirect("login.jsp");
	    }
		try {
			List<Request> queryDetails = RequestDao.fetchAllData();
			for(Request reqObject : queryDetails) {
				out.print(reqObject.getId() + "\t"
						+ reqObject.getName() + "\t"
						+ reqObject.getEmail() + "\t"
						+ reqObject.getPhone() + "\t"
						+ reqObject.getMessage() + "\t"
						+ reqObject.getIsActive());
				out.println();
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
}
