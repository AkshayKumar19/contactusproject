package com.akshay;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import com.akshay.classFiles.Request;
import com.akshay.dao.RequestDao;

@WebServlet("/SearchServlet")
public class SearchServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setHeader("cache-control", "no-cache, no-store, must-revalidate");
		response.setHeader("Pregme", "no-cache");
		response.setHeader("Expires", "0");
		
		HttpSession session = request.getSession();
		if(session.getAttribute("user") == null) {
			response.sendRedirect("login.jsp");
		}
		Boolean active = Boolean.parseBoolean(request.getParameter("isActive"));
		try {
			searchByIsActive(request, response, active);
		} catch (ClassNotFoundException | SQLException | IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void searchByIsActive(HttpServletRequest req, HttpServletResponse res, Boolean active) throws ClassNotFoundException, SQLException, IOException {
		PrintWriter out = res.getWriter();
		List<Request> activeStatusData = RequestDao.searchByStatus(active);
		
		for(Request reqClass : activeStatusData) {
			out.print(reqClass.getId() + "\t"
					+ reqClass.getName() + "\t"
					+ reqClass.getEmail() + "\t"
					+ reqClass.getPhone() + "\t"
					+ reqClass.getIsActive());
			out.println();
		}
	}
}