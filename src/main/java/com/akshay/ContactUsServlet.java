package com.akshay;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import com.akshay.classFiles.Request;
import com.akshay.dao.RequestDao;

@WebServlet("/ContactUsServlet")
public class ContactUsServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher reqDispatch = request.getRequestDispatcher("contactUs.jsp");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		String message = request.getParameter("message");
		
		Request re = new Request();
		re.setName(name);
		re.setEmail(email);
		re.setPhone(phone);
		re.setMessage(message);
		re.setIsActive(true);
		
		try {
			int status = RequestDao.saveRequest(re);
			if(status > 0) {
				String statusMessage = "Thanks for your query. We'll get back to you shortly!";
				request.setAttribute("statusMessage", statusMessage);
				reqDispatch.forward(request, response);
			}
			else {
				String statusMessage = "Server Error! Please, Try again later.";
				request.setAttribute("statusMessage", statusMessage);
				reqDispatch.forward(request, response);
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

}
